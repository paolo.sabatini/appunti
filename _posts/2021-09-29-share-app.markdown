---
title:  "Progetto: share screen web app"
date:   2021-09-29 19:00:00 +0100
layout: single

tagline: " "

categories: progetto lezione
tags : 5B 
---


Come realizzare un servizio di condivisione dello schermo da usare in rete locale?
L'app deve essere user friendly  ...

usare vnc server e client su ogni computer funziona ma i punti deboli sono:

-chi vuole condividere lo schermo deve far partire il server, controllare la sua configurazione, comunicare a tutti l'indirizzo ip...
-chi vuole accedere deve far partire vnc viewer ricordare l'indirizzo ip comunicato (che ha detto ? 192.168. che? )

Complicato !!

proposta semplificare il tutto con una web app :

l'utente senza tante complicazioni acceda ad un sito ...

fa una sorta di login (semplice tipo scelta di nick name) entra nel "meeting" e decide se condifvidere lo schermo...


Abbozzo :

ingriedenti : 

architettura client-server?
a naso mi sembra di si ...

lato client ? bisogna certamnet conoscere htm,css (neanche tanto..) javascript.
lato server ? potremmo svilupparlo in vari modi ...php? Asp? C# ? nodeJS? ...



ovvio che in quasi sett miliardi di persone al mondo non siamo i primi a porci il problema ...

Qui un interessante caso di studio :

[https://itsallbinary.com/create-your-own-screen-sharing-web-application-using-java-and-javascript-webrtc/](https://itsallbinary.com/create-your-own-screen-sharing-web-application-using-java-and-javascript-webrtc/)
