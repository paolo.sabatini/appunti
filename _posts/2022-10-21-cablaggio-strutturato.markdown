---
title:  "cablaggio strutturato"
date:   2022-10-21 08:00:00 +0100
layout: single

tagline: "prova post "

categories: lezione
tags : cablaggio 
---
standard de facto topologia cablaggio:  a stella!!

video introduttivo:

[Three basic components of Structured Cabling  https://www.youtube.com/watch?v=lAOE2aciNmw](https://www.youtube.com/watch?v=lAOE2aciNmw)

come la topologia a stella viene realizzata 


[Structured Cabling Beyond a Star-Topology Network  https://youtu.be/OkmQiKMKM9c](https://youtu.be/OkmQiKMKM9c)

osservazione : le "room" sono spesso e volentieri sostituite da armardi rack 

un video un po' più teorico ...
[What is Structured Cabling Standard (TIA-568-C)? https://youtu.be/NRE6O_mvFus](https://youtu.be/NRE6O_mvFus)

============================

Armadi Rack 
[https://www.manhattanshop.it/networking/armadi-e-accessori-rack/shopby/tipologia-a_muro-a_pavimento.html](https://www.manhattanshop.it/networking/armadi-e-accessori-rack/shopby/tipologia-a_muro-a_pavimento.html)

patch panel :

questo è uno con le prese fisse
   https://www.manhattanshop.it/pannello-patch-per-rack-10-12-porte-utp-cat-6-1he.html
qui come si cabla: https://www.youtube.com/watch?v=TgvmM6R8rQc

Quelli in laboratorio sono patch panel per frutti keystone jack più comodi da installare ..
frutto keystone : https://www.manhattanshop.it/frutto-keystone-rj45-cat-6-utp-tooless.html

patch panel per keystone : 
https://www.manhattanshop.it/pannello-patch-modulare-24-posti-rj45-keystone-utp-1he-grigio.html

https://www.youtube.com/watch?v=LzND7Y8PgaA

Qui una "drittata" : 

https://www.youtube.com/watch?v=nn0lL_sIeX0

Qui un altro video ..
https://www.youtube.com/watch?v=6GwMeaQqeNE

e un altro ancora:


https://www.youtube.com/watch?v=YgBSpYiJQHM
